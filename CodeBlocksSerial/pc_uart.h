#ifndef __PC_UART_H
#define __PC_UART_H

#define RX_BUFFER_SIZE 256

//UART Parameters

#define UART_BAUD       9600
#define UART_BYTE_SIZE  8
#define UART_STOP       ONESTOPBIT
#define UART_PARITY     NOPARITY

//#define UART_BAUD       115200
//#define UART_BYTE_SIZE  8
//#define UART_STOP       TWOSTOPBITS
//#define UART_PARITY     EVENPARITY

// Leave these defined if you want to see the TX and RX on console
// Comment out the preprocessor directive to quite outputs.
#define DEBUG_SHOW_WRITE
#define DEBUG_SHOW_READ

//Global Variable Definitions
unsigned char SerialBuffer[RX_BUFFER_SIZE];     //Buffer for storing Rxed Data
volatile int UARTexitFlag;

//Interrupt Vector
void intHandler(int dummy);

//Function Prototypes
void openComm(HANDLE*, int comInt);
BOOL setSerialParams(HANDLE*, DCB*);
void setTimeouts(HANDLE*, COMMTIMEOUTS*);
BOOL writeComm(HANDLE* phComm, char* writeString);
void readComm(HANDLE* phComm, char* readString);

#endif // __PC_UART_H
