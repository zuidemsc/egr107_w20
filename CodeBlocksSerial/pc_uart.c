#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <signal.h>
#include "pc_uart.h"


void intHandler(int dummy)
{
    UARTexitFlag = 1;
}

/*  Opens a communication port hComm on COMM Port comInt*/
void openComm(HANDLE* hComm, int comInt)
{
    char portStr[13];
    if(comInt < 10)
    {
        sprintf(portStr, "COM%d",comInt);
        *hComm = CreateFile(portStr,                  // port name
                        GENERIC_READ | GENERIC_WRITE, // Read/Write
                        0,                            // No Sharing
                        NULL,                         // No Security
                        OPEN_EXISTING,                // Open existing port only
                        0,                            // Non Overlapped I/O
                        NULL);                        // Null for Comm Devices
    }
    else {
        sprintf(portStr, "\\\\.\\COM%d", comInt);
        *hComm = CreateFile(portStr,                  //port name
                        GENERIC_READ | GENERIC_WRITE, //Read/Write
                        0,                            // No Sharing
                        NULL,                         // No Security
                        OPEN_EXISTING,                // Open existing port only
                        0,                            // Non Overlapped I/O
                        NULL);                        // Null for Comm Devices
    }

    if (*hComm == INVALID_HANDLE_VALUE)
    {
        printf("Error in opening serial port...Exiting\n\n");
        exit(0);
    }
    else
    {
        printf(portStr);
        printf(" serial port opened successfully\n\n");
    }

}

/*  Sets the communication parameters for the opened COMM Port*/
BOOL setSerialParams(HANDLE* phComm, DCB* pdcbSerialParams)
{
    BOOL status;
    pdcbSerialParams->DCBlength = sizeof(*pdcbSerialParams);

    status = GetCommState(*phComm, pdcbSerialParams);

    pdcbSerialParams->BaudRate = UART_BAUD;        // Setting BaudRate = 9600
    pdcbSerialParams->ByteSize = UART_BYTE_SIZE;   // Setting ByteSize = 8
    pdcbSerialParams->StopBits = UART_STOP;        // Setting StopBits = 1
    pdcbSerialParams->Parity   = UART_PARITY;      // Setting Paprintf("Error in opening serial port\n\n");rity = None

    SetCommState(*phComm, pdcbSerialParams);

    return status;
}

/* How long before timeouts...messing with these may cause instability*/
void setTimeouts(HANDLE* phComm, COMMTIMEOUTS* ptimeouts )
{
    ptimeouts->ReadIntervalTimeout         = 50; // in milliseconds
    ptimeouts->ReadTotalTimeoutConstant    = 50; // in milliseconds
    ptimeouts->ReadTotalTimeoutMultiplier  = 10; // in milliseconds
    ptimeouts->WriteTotalTimeoutConstant   = 50; // in milliseconds
    ptimeouts->WriteTotalTimeoutMultiplier = 10; // in milliseconds

    SetCommTimeouts(*phComm, ptimeouts);
}

/* Write to UART*/
BOOL writeComm(HANDLE* phComm, char* writeString)
{
    DWORD dNoOfBytestoWrite = strlen(writeString);
    DWORD dNoOfBytesWritten = 0;            // No of bytes written to the port
    BOOL status;

    status = WriteFile(*phComm,             // Handle to the Serial port
            writeString,                    // Data to be written to the port
            dNoOfBytestoWrite,              // No of bytes to write
            &dNoOfBytesWritten,             // Bytes written
            NULL);

    #ifdef DEBUG_SHOW_WRITE
    if(status)
        printf("__Dbug TX: %s\n", writeString);
    #endif

    if(!status)
        printf("Failed to write to comm\n");

    return status;
}

/*Read from UART*/
void readComm(HANDLE* phComm, char* readString)
{
    unsigned char TempChar;                 //Temporary character used for reading
    BOOL status;
    DWORD NoBytesRead;
    int i;

    //Clear read buffer
    for(i=0;i<RX_BUFFER_SIZE;i++)
        readString[i] = 0x00;

    i=0;
    do
    {
        status = ReadFile( *phComm,         // Handle of the Serial port
             &TempChar,                     // Temporary character
             1,                             // Size of TempChar
             &NoBytesRead,                  // Number of bytes read
             NULL);

        if(status && NoBytesRead)
        {
            readString[i] = TempChar;       // Store Tempchar into buffer
            i++;
        }
        else if(!status)
        {
            printf("Failed to read from comm\n");
            break;
        }
    }while (NoBytesRead > 0);

    #ifdef DEBUG_SHOW_READ
    if(status && readString[0] != 0x00)
        printf("__Dbug RX: %s\n", readString);
    #endif
}


