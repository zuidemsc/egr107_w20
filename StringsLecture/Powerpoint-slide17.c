#include <stdio.h>
#include <string.h>

int main()
{
    char s[81];
    strcpy(s, "Hello World!");
    puts(s);
    strcpy(s, "Goodbye World!");
    puts(s);
    s[0] = 'C';
    s[1] = 'R';
    s[2] = 'A';
    s[3] = '\0';
    
//    s = "Crash";  // Don't do this
    puts(s);
    return 0;
}
