#include <stdio.h>
#include <ctype.h>
#include <string.h>

int main()
{

    char s[] = "Rain in Spain!";
    int j;

    for(j=0; j < strlen(s); j++)
    {
        s[j] = toupper(s[j]);
        putchar(s[j]);
        putchar(' ');
        if(isspace(s[j])) puts("space character");
        if(ispunct(s[j])) puts("punctuation character");
        if(isalpha(s[j])) puts("letter character");
        putchar('\n');
    }

}
