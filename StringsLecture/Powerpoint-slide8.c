#include <stdio.h>

int main()
{
//     int data[] = {4,5,6,7,8};  //size 5 since no null for int arrays
    char str1[] = "Hello";  //first time only, string size of 6 created
    //str = "Hello";//this does not work.  Don't do this.
	int j;
	for(j=0;j<5;j++)
        printf("%c",str1[j]);
	printf("%s", str1);
    return 0;
}

int isalpha(char c)
{
 if((c <= 'Z' && c>='A') || (c<= 'z' && c>= 'a'))
     return 1;
 return 0;
}
