#include <stdio.h>
#include <string.h>

int main()
{

    char socsecurity[12] = "123-45-6789";
    char ssnshort[7], ssn1[4], ssn2[3], ssn3[5];

    strncpy(ssnshort,socsecurity,6);
    ssnshort[6] = '\0';

    strncpy(ssn1,socsecurity,6);
    ssn1[3] = '\0';

    strncpy(ssn2,&socsecurity[4],2);
    ssn2[2] = '\0';

    strncpy(ssn3,&socsecurity[7],4);
    ssn3[4] = '\0';

    printf("ssnshort = %s\n",ssnshort);
    printf("ssn1 = %s\n",ssn1);
    printf("ssn2 = %s\n",ssn2);
    printf("ssn3 = %s\n",ssn3);
    return 0;
}
