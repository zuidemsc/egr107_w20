#include <stdio.h>

int main()
{
    char s1[6];
    s1[0] = 'E';
    s1[1] = 'G';
    s1[2] = 'R';
    s1[3] = '1';
    s1[4] = '0';
    s1[5] = '7';
    s1[6] = '\0';  //This memory location is not in the array
    char s2[6];
    s2[0] = 'F';
    s2[1] = 'A';
    s2[2] = 'S';
    s2[3] = '\0';
    s2[4] = '5';
    s2[5] = '0';
    int data[100];
    data[0] = 40;
    /*The array s1 is not a string � why?

    What happens when we say:*/
    printf("s1=%s s2=%s\n", s1,s2);
    return 0;
}
