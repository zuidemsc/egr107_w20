#include <stdio.h>
int main()
{
    char c;
    c = getchar();
    while (c != EOF)
    {
        printf("%c ", c);
        c = getchar();
    }
    return 0;
}

/*
int main()
{
    int george[10];
    char data[6];  //data is a pointer to the array of 6 chars
    data[0] = 'h';
    data[1] = 'e';
    data[2] = 'l';
    data[3] = 'l';
    data[4] = 'o';
    data[5] = '\0';
    puts(data);
}*/
/*
void puts(char *string)
{
    int i =0;
    while(string[i] != '\0')
    {
        putc(string[i]);
        i++;
    }
}*/
