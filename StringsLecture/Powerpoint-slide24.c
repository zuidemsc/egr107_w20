#include <stdio.h>
#include <string.h>

int main()
{
    char s5[5], s10[10], s20[20];
    char aday[7] = "Sunday";
    char another[9] = "Saturday";
    int len;
//1)
    strncpy(s5, another, 4); s5[4] = '\0';
    printf("%s\n",s5);
//2)
    strcpy(s10, &aday[3]);
    printf("%s\n",s10);
//3)
    len = strlen(another);
    printf("%d\n",len);
//4)
    strcpy(s20,aday); strcat(s20,another);
    printf("%s\n",s20);

    return 0;
}
