#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main()
{
    char a[] = "aB \t,.";
    char b[] = "3.1415";
    char c[] = "411";
    int i = 0;

    for(i=0; i<strlen(a); i++)
    {
        printf("\"%c\"  Alpha=%d, Space=%d, Punct=%d\n",a[i],isalpha(a[i]),isspace(a[i]),ispunct(a[i]));
        tolower(a[i]);
        printf("lower = %c\n",a[i]);
        toupper(a[i]);
        printf("upper = %c\n",a[i]);
    }

    printf("double %s = %lf\n",b,atof(b));
    printf("double %s = %lf\n",c,atof(c));

    printf("int %s = %d\n",b,atoi(b));
    printf("int %s = %d\n",c,atoi(c));


    return(0);
}
