#include <stdio.h>
#include <string.h>

int main()
{
    int result;
	char s1[21], s2[21];

	strcpy(s1, "JONES");
	strcpy(s2, "jones");
	result = strcmp(s1, s2);		// is result >, < or = to zero?
	printf("%s compared to %s is %d\n",s1,s2,result);

	strcpy(s1, "Jones");
	strcpy(s2, "Janes");
	result = strcmp(s1, s2);		// is result >, < or = to zero?
	printf("%s compared to %s is %d\n",s1,s2,result);

	strcpy(s1, "Jones");
	strcpy(s2, "Joneses");
	result = strcmp(s1, s2);	// is result >, < or = to zero?
	printf("%s compared to %s is %d\n",s1,s2,result);

	strcpy(s1, "Jazzz");
	strcpy(s2, "JoAAA");
	result = strcmp(s1, s2);		// is result >, < or = to zero?
	printf("%s compared to %s is %d\n",s1,s2,result);

	strcpy(s1, "j");
	strcpy(s2, "Jones");
	result = strcmp(s1, s2);		// is result >, < or = to zero?
	printf("%s compared to %s is %d\n",s1,s2,result);
}
