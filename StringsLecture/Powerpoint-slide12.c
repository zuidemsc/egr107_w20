#include <stdio.h>

int main()
{
    char s1[25], s2[25];
    scanf("%s %s", s1, s2);
    /*Assume the user types:
    Hello World
    Results in
    */
    printf("%s %s\n", s1, s2);

    /*C just used the elements it needed, the others have garbage in them
    C added the NULL
    What if your input statement was*/ scanf("%s", s1);// ?
    printf("%s %s\n", s1, s2);
}
