#include <stdio.h>
#include <string.h>

void somefun();

int main()
{
    char a[] = "Hello";
    char b[] = "Hello";

    printf("strcmp(a, b)\n");
    if(strcmp(a, b))
        somefun();

    printf("strcmp(a, b) == 0\n");
    if(strcmp(a, b) == 0)
        somefun();

    printf("a == b\n");
    if(a == b)
        somefun();

    printf("&a[0] == &b[0]\n");
    if(&a[0] == &b[0])
        somefun();

}

void somefun() {
    printf("Comparison True\n");
}
