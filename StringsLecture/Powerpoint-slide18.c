#include <stdio.h>
#include <string.h>
int makeYourOwnstrlen(char *str);
char *makeYourOwnstrcpy(char *str1,char *str2);

int main()
{
    char s[81];
    int n;
    strcpy(s, "Hello Bill");
    printf("%s\n", s);
    n = strlen(s);  // find the string length
    printf("%d\n", n);
    n = makeYourOwnstrlen(s);  // find the string length
    printf("%d\n", n);
    makeYourOwnstrcpy(s, "Goodbye Bill");
    printf("%s\n", s);
    return 0;
}

int makeYourOwnstrlen(char *str)
{
    int len = 0; 
    while(str[len] != '\0')
        len++;
    return len;
}

char *makeYourOwnstrcpy(char *str1,char *str2)
{
    int pos = 0;
    while(str2[pos] != '\0')
    {
        str1[pos] = str2[pos];
        pos ++;
    }
    str1[pos] = '\0';
    return str1;
}
