#include <stdio.h>
#include <string.h>

int main()
{
    char str1[80] = "Hello Goodbye. Yes, No; maybe.  Is this helpful?";
	char str2[80] = " ,;.";
	char *str3;

    //0 == NULL == '\0';
	str3 = strtok(str1,str2);  // first time call
    //What str1 is currently = Hello\0Goodbye. Yes, No; maybe.  Is this helpful?;
	while(str3) {
		printf("str1 Word = %s, str3 Word = %s\n",str1,str3);
		str3 = strtok(NULL,str2);  // all other times through the same string
        //What str1 is currently = Hello\0Goodbye\0\0Yes, No; maybe.  Is this helpful?;
	}

	return 0;
}

char *makeyourownStrTok(char *str1, char *str2)
{
    char *str3;
    static int pos = 0;
    static char *memoryPos;
    
    if(str1 != NULL)
    {
        str3 = str1;
        memoryPos = str1;
        while(str1[pos] != str2[0])  //str2 only has one char to compare in example
        {
            pos++;
        }
        str1[pos] = '\0';
    }
    else
    {
        str3 = &(memoryPos[pos+1]);
        while(memoryPos[pos] != str2[0])  //str2 only has one char to compare in example
        {
            pos++;
        }
        memoryPos[pos] = '\0';
    }
    return str3;
}
